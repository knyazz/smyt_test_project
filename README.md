INSTALL

activate virtualenv and install requirements.

```
pip install -r requirements.txt
./manage.py makemigrations
./manage.py migrate
./manage.py createsuperuser
./manage.py runserver
```

in settings.py:
```
DYNAMIC_MODELS_CONFIG_FILEPATH = 'base/models_desc.yaml'
MAX_LENGHT_CHAR = 255
```