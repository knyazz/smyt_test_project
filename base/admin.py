from django.contrib import admin

from .models import create_dynamic_models

for model in create_dynamic_models.registry:
    admin.site.register(model)
