#coding: utf-8
from __future__ import unicode_literals

import json
from django.forms.models import ModelFormMetaclass, ModelForm
from django.db.models import get_model
from django.views.generic.base import TemplateView
from django.http import HttpResponse, Http404

from rest_framework import generics

from .models import create_dynamic_models


class ModelObjMixin(object):
    def dispatch(self, request, *args, **kwargs):
        model_name = kwargs.get('model_name')
        self.model = get_model('base', str(model_name))
        return super(ModelObjMixin, self).dispatch(request, *args, **kwargs)


class ModelObjList(ModelObjMixin, generics.ListCreateAPIView):
    b''' список объектов модели и создание нового объекта '''
get_data = ModelObjList.as_view()


class ModelObjUpdate(ModelObjMixin, generics.UpdateAPIView):
    b''' обновление обекта модели'''
modelobj_update = ModelObjUpdate.as_view()

def get_fields(request, model_name):
    model = get_model('base', str(model_name))
    if model:
        formname = str(model_name)+'Form'
        attrs = {}
        attrs['Meta'] = type(str('Meta'), (), dict(model=model))
        form = ModelFormMetaclass(str(formname), (ModelForm,), attrs)
        fields = [{'name':f.verbose_name, } for f in model._meta.fields]
        model_desc = {  'fields': fields, 
                        'form': form().as_p(),
                        'modelname':str(model_name)
        }
        return HttpResponse(json.dumps(model_desc), content_type="application/json")
    else:
        raise Http404


class IndexPage(TemplateView):
    template_name = 'index.html'
    def get_context_data(self, **kwargs):
        ctx = super(IndexPage, self).get_context_data(**kwargs)
        ctx['models'] = [(model.__name__, model._meta) for model in create_dynamic_models.registry]
        return ctx
index = IndexPage.as_view()