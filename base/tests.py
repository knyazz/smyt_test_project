# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import get_user_model
User = get_user_model()
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models import get_model
from django.test.client import Client

from django_nose.testcases import FastFixtureTestCase


class BaseTest(FastFixtureTestCase):
    fixtures = ('smyt_test_project/fixtures/init.json',)
    def setUp(self):
        self.test_client = Client()

    @classmethod
    def _fixture_teardown(cls):
        if settings.DATABASES.get('default', {}
                            ).get('ENGINE') != 'django.db.backends.sqlite3':
            fixtures = getattr(cls, 'fixtures', None)
            _fb_should_teardown_fixtures = getattr(cls, '_fb_should_teardown_fixtures', True)
            # _fixture_teardown needs help with constraints.
            if fixtures and _fb_should_teardown_fixtures:
                call_command('flush', interactive=False, verbosity=1)
        return super(BaseTest, cls)._fixture_teardown()

    def get_simple_page(self, url, kw=None):
        kw = kw or {}
        response = self.test_client.get(reverse(url, kwargs=kw))
        self.assertEqual(response.status_code, 200)
        return response

    def get_simple_pages(self, url_kw_dict):
        for url,kw in url_kw_dict.items():
            self.get_simple_page(url,kw)

    def get_list_and_detail_pages(self, url_dict):
        for obj_list_url, obj_detail_url in url_dict.items():
            # get obj_list page
            response = self.get_simple_page(obj_list_url)
            ad_pk = response.context[0].get('object_list')[0].pk
            # get obj detail page
            self.get_simple_page(obj_detail_url, kw=dict(pk=ad_pk))     

    def create_page(self, url, params=None, kw=None):
        kw = kw or {}
        params = params or {}
        response = self.test_client.post(reverse(url, kwargs=kw), params)
        self.assertEqual(response.status_code, 201)
        return response

    def logining(self):
        transaction.set_autocommit(True)

    def create_model_object(self, model_name, params=None, kw=None):
        kw = kw or {}
        params = params or {}
        model = get_model('base', str(model_name))
        obj = model.objects.create(**params)
        obj.save()
        self.assertIsNotNone(obj.id)
        return obj.id


class BaseAPITest(BaseTest):
    def base_simple_tests(self):
        '''
            test base functionality
        '''
        self.logining()
        url_kw_dict = {
                    'index': {},
                    'get_fields': {'model_name':'rooms'},
                    'get_fields': {'model_name':'users'},
                    'get_data': {'model_name':'rooms'},
                    'get_data': {'model_name':'users'},
                }
        self.get_simple_pages(url_kw_dict)

        #url_dict = {
            #'base:ads_list': 'base:ads',
        #}
        #self.get_list_and_detail_pages(url_dict)

        #create
        params = {
                    'paycheck': 1,
                    'name': 1,
                    'date_joined': '2014-08-19'
        }
        modelobjid = self.create_model_object('users',params)
        url = 'modelobj_update'
        self.test_client.post(reverse(url, kwargs=dict(pk=modelobjid,
                                                    model_name='users'),
                                    ),
                                params) 
        self.create_page('get_data', params, kw={'model_name':'users'})
        params = {
                    'department': 1,
                    'spots': 1,
        }
        modelobjid = self.create_model_object('rooms',params)
        self.test_client.post(reverse(url, kwargs=dict(pk=modelobjid,
                                                    model_name='rooms'),
                                    ),
                                params) 
        self.create_page('get_data', params, kw={'model_name':'rooms'})