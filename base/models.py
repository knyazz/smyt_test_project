#coding: utf-8
from __future__ import unicode_literals

import yaml

from django.conf import settings
from django.db import models


class CreateDynModels(object):
    b'''класс создания динамических моделей'''
    registry = None

    def __init__(self, *args, **kwargs):
        super(CreateDynModels, self).__init__(*args, **kwargs)
        self.models_desc = self.parse_dynmodels_config()
        self.create_models()

    def parse_dynmodels_config(self):
        _config_filepath = getattr(settings, 'DYNAMIC_MODELS_CONFIG_FILEPATH', None)
        if _config_filepath:
            with open(_config_filepath, 'r') as stream:
                return yaml.load(stream)

    def create_models(self):
        if self.models_desc:
            self.registry = self.registry or []
            for name, desc in self.models_desc.items():
                fields = desc.get('fields')
                attrs = {'__module__': self.__module__}
                attrs.update(self.get_fields(fields))
                attrs['Meta'] = self.get_meta_class(desc.get('title', name))
                new_class = models.base.ModelBase(name, (models.Model,), attrs)
                self.registry.append(new_class)

    def get_fields(self, fields=None):
        fields = fields or []
        res = {}
        for field in fields:
            field_type = {
                            'int': models.IntegerField,
                            'date': models.DateField,
                            'char': models.CharField
            }.get(field.get('type'))
            if field_type:
                if field.get('type') == 'char':
                    ml = settings.MAX_LENGHT_CHAR
                    res[field.get('id')] = field_type(field.get('title'),
                                                      max_length=ml)
                else:
                    res[field.get('id')] = field_type(field.get('title'))
        return res

    def get_meta_class(self, name):
        return type(str('Meta'), (), dict(verbose_name=name, 
                                          verbose_name_plural=name)
        )
create_dynamic_models = CreateDynModels()