from django import template
from base.models import create_dynamic_models

register = template.Library()


@register.inclusion_tag('dummy.html', takes_context=True)
def dynamic_tabs(context, template='dynamic_tabs.html'):
    """Return model_name, model.verbose_name dict on dymanic models"""
    models = {}
    for model in create_dynamic_models.registry:
        models.update({model._meta.model_name: model._meta.verbose_name})
    return {'template': template,'models': models,}