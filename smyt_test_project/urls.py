from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'base.views.index', name='index'),
    
    url(r'^fields/(?P<model_name>[\w]+)/$', 
        'base.views.get_fields', 
        name='get_fields'
    ),
    
    url(r'^(?P<model_name>[\w]+)/$', 'base.views.get_data', name='get_data'),
    url(r'^(?P<model_name>[\w]+)/(?P<pk>\d+)/$', 
        'base.views.modelobj_update', 
        name='modelobj_update'),
)
